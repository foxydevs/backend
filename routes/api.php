<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('interest', 'UserInterestController');
Route::resource('users', 'UsersController');
Route::resource('friends', 'FriendsController');
Route::resource('events', 'EventsController');

Route::get('users/{friend}/friends/{id}', 'UsersController@showAllUserInformation');
Route::get('users/{id}/friends', "FriendsController@getFriends");
Route::get('users/{id}/nofriends', "FriendsController@getNoFriends");
Route::get('users/{id}/invitations', "FriendsController@getInvitations");
Route::post('upload/avatar/{id}', 'UsersController@uploadAvatar');

Route::post('login', 'AuthenticateController@login');
Route::get('logout', 'AuthenticateController@logout');

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
