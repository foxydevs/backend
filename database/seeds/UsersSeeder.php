<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username'          => 'alex',
            'password'          => bcrypt('alex'),
            'email'             => 'alexmejicanos@outlook.com',
            'firstname'         => 'Alex',
            'lastname'          => 'Mejicanos',
            'work'              => 'Desarrollador Software',
            'description'       => 'Hola soy un desarrollador de software',
            'age'               => 23,
            'birthday'          => '1993-02-27',
            'phone'             => '+502 30358525',
            'picture'           => 'http://52.89.117.243/avatar/alex.jpg',
            'last_conection'    => date('Y-m-d H:m:s'),
            'facebook_id'       => '',
            'last_latitud'      => 0.0,
            'last_longitud'     => 0.0,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('users')->insert([
            'username'          => 'stevejobs',
            'password'          => bcrypt('stevejobs'),
            'email'             => 'steeve@apple.com',
            'firstname'         => 'Steve',
            'lastname'          => 'Jobs',
            'work'              => 'Empresario',
            'description'       => 'Fundador de Apple, Next, Pixar y muchas otras compañias.',
            'age'               => 56,
            'birthday'          => '1955-02-24',
            'phone'             => '+101 2345432',
            'picture'           => 'http://52.89.117.243/avatar/stevejobs.jpg',
            'last_conection'    => date('Y-m-d H:m:s'),
            'facebook_id'       => '',
            'last_latitud'      => 0.0,
            'last_longitud'     => 0.0,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('users')->insert([
            'username'          => 'billgates',
            'password'          => bcrypt('billgates'),
            'email'             => 'bill@microsoft.com',
            'firstname'         => 'Bill',
            'lastname'          => 'Gates',
            'work'              => 'Empresario',
            'description'       => 'Fundador de Microsoft y actualmente el segundo hombre mas rico del mundo.',
            'age'               => 60,
            'birthday'          => '1955-10-28',
            'phone'             => '+102 765345',
            'picture'           => 'http://52.89.117.243/avatar/billgates.JPG',
            'last_conection'    => date('Y-m-d H:m:s'),
            'facebook_id'       => '',
            'last_latitud'      => 0.0,
            'last_longitud'     => 0.0,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('users')->insert([
            'username'          => 'galgadot',
            'password'          => bcrypt('galgadot'),
            'email'             => 'galgadot@me.com',
            'firstname'         => 'Gal',
            'lastname'          => 'Gadot',
            'work'              => 'Actriz/Modelo',
            'description'       => 'Actriz y Modelo en Holliwod, la Mujer Maravilla.',
            'age'               => 31,
            'birthday'          => '1985-04-30',
            'phone'             => '+201 674883',
            'picture'           => 'http://52.89.117.243/avatar/galgadot.jpeg',
            'last_conection'    => date('Y-m-d H:m:s'),
            'facebook_id'       => '',
            'last_latitud'      => 0.0,
            'last_longitud'     => 0.0,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
    }
}
