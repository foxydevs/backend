<?php

use Illuminate\Database\Seeder;

class EventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([
            'date'              => '2016-10-16',
            'time'              => '14:00:00',
            'picture'           => 'http://www.julios.co.za/wp-content/uploads/2012/10/restaurant.jpeg',
            'description'       => 'Almorzar en el restaurante las Margaritas Zona 10, conversar, conocernos y platicar de temas de politica y la situación del Pais.',
            'latitude'          => 14.599516,
            'longitude'         => -90.511892,
            'user_created'      => 1,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('events')->insert([
            'date'              => '2016-10-17',
            'time'              => '20:00:00',
            'picture'           => 'https://www.schick-hotels.com/files/images/content/f-b-restaurant-schick/restaurant-wien-schick.jpg',
            'description'       => 'Cenar en el restaurante las Margaritas Zona 10, conversar, conocernos y platicar de temas de politica y la situación del Pais.',
            'latitude'          => 14.599516,
            'longitude'         => -90.511892,
            'user_created'      => 2,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('user_events')->insert([
            'state'             => true,
            'event'             => 1,
            'user'              => 2,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('user_events')->insert([
            'state'             => false,
            'event'             => 1,
            'user'              => 3,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('user_events')->insert([
            'state'             => true,
            'event'             => 2,
            'user'              => 1,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

    }
}
