<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(UserInterestSeeder::class);
        $this->call(UserFriendsSeeder::class);
        $this->call(EventsSeeder::class);
    }
}
