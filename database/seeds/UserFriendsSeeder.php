<?php

use Illuminate\Database\Seeder;

class UserFriendsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('friends')->insert([
            'user_send'         => 1,
            'user_receipt'      => 2,
            'state'             => true,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('friends')->insert([
            'user_send'         => 4,
            'user_receipt'      => 1,
            'state'             => true,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('friends')->insert([
            'user_send'         => 1,
            'user_receipt'      => 3,
            'state'             => false,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('friends')->insert([
            'user_send'         => 2,
            'user_receipt'      => 3,
            'state'             => true,
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
    }
}
