<?php

use Illuminate\Database\Seeder;

class UserInterestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interest')->insert([
            'user'      => 1,
            'name'      => 'Comida',
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('interest')->insert([
            'user'      => 1,
            'name'      => 'Música',
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('interest')->insert([
            'user'      => 1,
            'name'      => 'Mujeres',
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('interest')->insert([
            'user'      => 2,
            'name'      => 'Apple',
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('interest')->insert([
            'user'      => 2,
            'name'      => 'Mac',
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('interest')->insert([
            'user'      => 2,
            'name'      => 'iPhone',
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('interest')->insert([
            'user'      => 3,
            'name'      => 'Microsoft',
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('interest')->insert([
            'user'      => 3,
            'name'      => 'Tablets',
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('interest')->insert([
            'user'      => 3,
            'name'      => 'Windows 10',
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('interest')->insert([
            'user'      => 4,
            'name'      => 'Comida',
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('interest')->insert([
            'user'      => 4,
            'name'      => 'Apple',
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);

        DB::table('interest')->insert([
            'user'      => 4,
            'name'      => 'Música',
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
    }
}
