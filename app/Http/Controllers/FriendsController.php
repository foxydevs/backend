<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Friends;
use App\Users;
use Response;
use Collection;
use Validator;

class FriendsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Friends::with('send','receipt')->get(), 200);
    }

    public function getFriends(Request $request, $id)
    {
        $user = Users::find($id);
        if ($user) {
            try {
                $senderUser  = Friends::whereRaw("user_send = ? AND state = 1", [$user->id])->with('receipt')->get();
                $receiveUSer = Friends::whereRaw("user_receipt = ? AND state = 1", [$user->id])->with('send')->get();

                $data = collect();

                foreach ($senderUser as $key => $value) {

                    $data->push($value->receipt);
                }

                foreach ($receiveUSer as $key => $value) {
                    $data->push($value->send);
                }

                return Response::json($data, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getNoFriends(Request $request, $id) {
        $user = Users::find($id);
        if ($user) {
            try {
                $senderUser  = Friends::whereRaw("user_send = ? AND state = 1", [$user->id])->with('receipt')->get();
                $receiveUSer = Friends::whereRaw("user_receipt = ? AND state = 1", [$user->id])->with('send')->get();

                $data = collect();

                foreach ($senderUser as $key => $value) {
                    $data->push($value->receipt->id);
                }

                foreach ($receiveUSer as $key => $value) {
                    $data->push($value->send->id);
                }

                $data->push($user->id);

                $nofriends = Users::whereNotIn("id", $data)->get();

                return Response::json($nofriends, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getInvitations(Request $request, $id)
    {
        $user = Users::find($id);
        if ($user) {
            try {
                $senderUser  = Friends::whereRaw("user_send = ? AND state = 0", [$user->id])->with('receipt')->get();
                $receiveUSer = Friends::whereRaw("user_receipt = ? AND state = 0", [$user->id])->with('send')->get();

                $data['send'] = $senderUser;
                $data['receive'] = $receiveUSer;

                return Response::json($data, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_send'     => 'required',
            'user_receipt'  => 'required'
        ]);

        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $evalObject = Friends::whereRaw('user_send = ? AND user_receipt = ?', [$request->get('user_send'), $request->get('user_receipt')])->first();
                $evalObject2 = Friends::whereRaw('user_send = ? AND user_receipt = ?', [$request->get('user_receipt'), $request->get('user_send')])->first();
                if ($evalObject || $evalObject2) {
                    $returnData = array (
                        'status' => 409,
                        'message' => 'Conflict with Entities'
                    );
                    return Response::json($returnData, 409);
                }
                else {
                    $newObject = new Friends();
                    $newObject->user_send = $request->get('user_send');
                    $newObject->user_receipt = $request->get('user_receipt');
                    $newObject->state = false;
                    $newObject->save();

                    return Response::json($newObject, 200);
                }
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Friends::find($id);
        if ($objectSee) {
            $objectSee->send;
            $objectSee->receipt;
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Friends::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->user_send = $request->get('user_send', $objectUpdate->user_send);
                $objectUpdate->user_receipt = $request->get('user_receipt', $objectUpdate->user_receipt);
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
                $objectUpdate->save();

                $objectUpdate->tipousuarios;
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Friends::find($id);
        if ($objectDelete) {
            try {
                Friends::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
