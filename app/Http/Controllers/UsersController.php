<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Users;
use App\Events;
use App\UserEvents;
use App\Friends;
use Response;
use Validator;
use Hash;
use Storage;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Users::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'      => 'required',
            'password'      => 'required',
            'email'         => 'required'
        ]);

        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Users();
                $newObject->username = $request->get('username');
                $newObject->password = Hash::make($request->get('password'));
                $newObject->email = $request->get('email');
                $newObject->firstname = '';
                $newObject->lastname = '';
                $newObject->work = '';
                $newObject->description = '';
                $newObject->age = 0;
                $newObject->birthday = null;
                $newObject->phone = '';
                $newObject->picture = '/avatar/no_avatar.png';
                $newObject->last_conection = date('Y-m-d H:m:s');
                $newObject->facebook_id = '';
                $newObject->last_latitud = 0.0;
                $newObject->last_longitud = 0.0;
                $newObject->save();
                return Response::json($newObject, 200);
            }
            catch(Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Users::find($id);
        if ($objectSee) {
            $objectSee->number_events_created = Events::where('user_created', '=', $objectSee->id)->count();
            $objectSee->number_events_assisted = UserEvents::whereRaw('user = ? AND state = 1', [$objectSee->id])->count();
            $objectSee->interests;
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function showAllUserInformation(Request $request, $friend, $id)
    {
        $me = Users::find($id);
        $friend = Users::find($friend);

        if ($me && $friend) {
            $senderUser_me  = Friends::whereRaw("user_send = ? AND state = 1", [$me->id])->with('receipt')->get();
            $receiveUSer_me = Friends::whereRaw("user_receipt = ? AND state = 1", [$me->id])->with('send')->get();

            $data_me = collect();

            foreach ($senderUser_me as $key => $value) {

                $data_me->push($value->receipt);
            }

            foreach ($receiveUSer_me as $key => $value) {
                $data_me->push($value->send);
            }

            $senderUser_friend  = Friends::whereRaw("user_send = ? AND state = 1", [$friend->id])->with('receipt')->get();
            $receiveUSer_friend = Friends::whereRaw("user_receipt = ? AND state = 1", [$friend->id])->with('send')->get();

            $data_friend = collect();

            foreach ($senderUser_friend as $key => $value) {

                $data_friend->push($value->receipt);
            }

            foreach ($receiveUSer_friend as $key => $value) {
                $data_friend->push($value->send);
            }

            $data = collect();

            foreach ($data_me as $key_me => $value_me) {
                foreach ($data_friend as $key_friend => $value_friend) {
                    if ($value_me->id == $value_friend->id) {
                        $data->push($value_friend);
                    }
                }
            }

            $friend->interests;
            $friend->similar = $data;
            return Response::json($friend, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Users::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->username = $request->get('username', $objectUpdate->username);
                $objectUpdate->email = $request->get('email', $objectUpdate->email);
                $objectUpdate->firstname = $request->get('firstname', $objectUpdate->firstname);
                $objectUpdate->lastname = $request->get('lastname', $objectUpdate->lastname);
                $objectUpdate->age = $request->get('age', $objectUpdate->age);
                $objectUpdate->birthday = $request->get('birthday', $objectUpdate->birthday);
                $objectUpdate->phone = $request->get('phone', $objectUpdate->phone);
                $objectUpdate->picture = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->last_conection = $request->get('last_conection', $objectUpdate->last_conection);
                $objectUpdate->facebook_id = $request->get('facebook_id', $objectUpdate->facebook_id);
                $objectUpdate->last_latitud = $request->get('last_latitud', $objectUpdate->last_latitud);
                $objectUpdate->last_longitud = $request->get('last_longitud', $objectUpdate->last_longitud);
                $objectUpdate->save();

                $objectUpdate->number_events_created = Events::where('user_created', '=', $objectUpdate->id)->count();
                $objectUpdate->number_events_assisted = UserEvents::whereRaw('user = ? AND state = 1', [$objectUpdate->id])->count();
                $objectUpdate->interests;
                return Response::json($objectUpdate, 200);
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
            }
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function uploadAvatar(Request $request, $id) {
        $objectUpdate = Users::find($id);
        if ($objectUpdate) {

            $validator = Validator::make($request->all(), [
                'avatar'      => 'required|image|mimes:jpeg,png,jpg'
            ]);

            if ($validator->fails()) {
                $returnData = array(
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
            else {
                try {
                    $path = Storage::disk('s3')->put('avatar', $request->avatar);

                    $objectUpdate->picture = Storage::disk('s3')->url($path);
                    $objectUpdate->save();

                    return Response::json($objectUpdate, 200);
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }

            }

            return Response::json($objectUpdate, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Users::find($id);
        if ($objectDelete) {
            try {
                Users::destroy($id);
                return Response::json($objectDelete, 200);
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
