<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Events;
use Response;
use Validator;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Events::with('user','assistants','pictures','comments')->get(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $validator = Validator::make($request->all(), [
            'date'          => 'required',
            'time'          => 'required',
            'description'   => 'required',
            'latitude'      => 'required',
            'longitude'     => 'required',
            'user_created'  => 'required',
        ]);

        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Events();
                $newObject->date            = $request->get('date');
                $newObject->time            = $request->get('time');
                $newObject->picture         = $request->get('picture', '');
                $newObject->description     = $request->get('description');
                $newObject->latitude        = $request->get('latitude');
                $newObject->longitude       = $request->get('longitude');
                $newObject->user_created    = $request->get('user_created');
                $newObject->save();
                return Response::json($newObject, 200);

            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Events::find($id);
        if ($objectSee) {
            $objectSee->user;
            $objectSee->assistants;
            $objectSee->pictures;
            $objectSee->comments;
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Events::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->date = $request->get('date', $objectUpdate->date);
                $objectUpdate->time = $request->get('time', $objectUpdate->time);
                $objectUpdate->picture = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->description = $request->get('description', $objectUpdate->description);
                $objectUpdate->latitude = $request->get('latitude', $objectUpdate->latitude);
                $objectUpdate->longitude = $request->get('longitude', $objectUpdate->longitude);
                $objectUpdate->user_created = $request->get('user_created', $objectUpdate->user_created);
                $objectUpdate->save();

                $objectUpdate->user;
                $objectUpdate->assistants;
                $objectUpdate->pictures;
                $objectUpdate->comments;
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Events::find($id);
        if ($objectDelete) {
            try {
                Events::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
