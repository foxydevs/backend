<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $table = 'events';

    public function user() {
        return $this->hasOne('App\Users', 'id', 'user_created');
    }

    public function assistants() {
        return $this->hasMany('App\UserEvents', 'event')->with('user');
    }

    public function pictures() {
        return $this->hasMany('App\PicturesEvents', 'event');
    }

    public function comments() {
        return $this->hasMany('App\CommentsEvents', 'event');
    }
}
