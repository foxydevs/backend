<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    protected $table = 'users';

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function interests(){
        return $this->hasMany('App\UserInterest','user');
    }
}
