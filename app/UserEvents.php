<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEvents extends Model
{
    protected $table = 'user_events';

    public function user(){
        return $this->hasOne('App\Users', 'id', 'user');
    }
}
